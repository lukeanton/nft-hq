import { useEffect, useState } from 'react';

import type { NextPage } from 'next';

import { ConsoleMeme } from '../components/ConsoleMeme';
import { HeroPanel } from '../components/HeroPanel/HeroPanel';
import { ImageLogo } from '../components/ImageLogo';
import { Loader } from '../components/Loader';
import { MainContent } from '../components/MainContentContainer';
import { pageConstants } from '../constants/page';

const Home: NextPage = () => {
  const { saucyEyeConspiracyTheroryParanoiaIsTakingOverIThinkIThinkIAmInLoveWithPutinsMomLoader } = pageConstants;
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [isImagesLoading, setIsImagesLoading] = useState<boolean>(true);

  useEffect(() => {
    //Simulating image fetch
    setTimeout(() => {
      setIsImagesLoading(false);
      setIsLoaded(true);
    }, 2000);
  }, []);

  return (
    <>
      <div className="w-full relative">
        <HeroPanel />
        <MainContent />
      </div>
      <Loader
        icon={
          <ImageLogo
            additionalClassNames="animate-pulse"
            size="large"
            src={saucyEyeConspiracyTheroryParanoiaIsTakingOverIThinkIThinkIAmInLoveWithPutinsMomLoader}
          />
        }
        isLoading={Boolean(isImagesLoading)}
      />
      {isLoaded && <ConsoleMeme />}
    </>
  );
};

// eslint-disable-next-line import/no-default-export
export default Home;
