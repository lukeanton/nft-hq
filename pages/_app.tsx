/* eslint-disable import/no-default-export */
import type { AppProps } from 'next/app';
import { ToastContainer } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';

import { Icons } from '../components';

import '../styles/globals.css';

import '../styles/imageHover.css';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Component {...pageProps} />
      <Icons />
      <ToastContainer position="top-center" theme="colored" />
    </>
  );
}

export default MyApp;
