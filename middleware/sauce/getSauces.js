import { gql } from '@apollo/react-hooks';

const GET_SAUCES = gql`
  query getSauces {
    examples {
      sauce
      id
    }
  }
`;

export { GET_SAUCES };
