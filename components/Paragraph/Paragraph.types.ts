export interface IParagraphProps {
  /**
   * italicsize that boi
   */
  italic?: boolean;
  /**
   * Paragraph text
   */
  paragraphText: string;
}
