import { IParagraphProps } from './Paragraph.types';

const Paragraph = ({ paragraphText, italic }: IParagraphProps) => (
  <p className={`text-md md:text-lg lg:text-2xl ${italic ? 'italic' : ''}`}>{paragraphText}</p>
);

export { Paragraph };
