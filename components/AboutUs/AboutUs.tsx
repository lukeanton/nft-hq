import { Paragraph } from '../Paragraph';
import { SectionTitle } from '../SectionTitle';

import { cms } from '../../constants/cms';

const AboutUs = () => {
  return (
    <div className="my-32">
      <SectionTitle title={cms.aboutUs.title} />
      <div className="">
        <div className="mb-8">
          <Paragraph paragraphText={cms.aboutUs.paragraph1} />
        </div>
        <div className="mb-8">
          <Paragraph paragraphText={cms.aboutUs.paragraph2} />
        </div>
      </div>
    </div>
  );
};

export { AboutUs };
