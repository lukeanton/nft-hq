import { useEffect, useState } from 'react';

import { AboutUs } from '../AboutUs';
import { DividerBar } from '../DividerBar';
import { HowItWorks } from '../HowItWorks';
import { ImageGallery } from '../ImageGallery';
import { IntroducingTheLivingNFT } from '../IntroducingTheLivingNFT';
import { ProjectRoadmap } from '../ProjectRoadmap';

const MainContent = () => {
  const [containerStart, setContainerStart] = useState<number>(0);

  const handleResize = () => {
    const headline = document.getElementById('headlineWhiteOutline');
    const logo = document.getElementById('logoContainer');
    setContainerStart((logo?.clientHeight ?? 0) + (headline?.clientHeight ?? 0) + 90);
  };

  useEffect(() => {
    handleResize();
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div className="relative mt-10" id="mainContainer" style={{ marginTop: `${containerStart}px` }}>
      <div className="text-center px-8 md:px-32 lg:px-64">
        <ImageGallery galleryTitle="Bape" />
        <DividerBar />
        <HowItWorks />
        <DividerBar />
        <IntroducingTheLivingNFT />
        <DividerBar />
        <AboutUs />
        <DividerBar />
      </div>
      <div className="text-center">
        <ProjectRoadmap />
      </div>
      <div className="text-center px-8 md:px-32 lg:px-64">
        <DividerBar />
      </div>
    </div>
  );
};

export { MainContent };
