import { useState, useEffect } from 'react';

import { ApolloError } from '@apollo/client';
import { toast } from 'react-toastify';

import { Loader } from '../Loader';

import { ImageGalleryProps, IGalleryImage } from './ImageGallery.interfaces';

import { useGetImagesByType, IGetImageByTypeOnCompletedResponse } from '../../hooks';

const ImageGallery = ({ galleryTitle }: ImageGalleryProps) => {
  const [galleryImages, setGalleryImages] = useState<IGalleryImage[]>();

  const handleGetImagesByTypeComplete = (data?: IGetImageByTypeOnCompletedResponse) => {
    if (!data) {
      return;
    }

    const { images } = data;

    const returnedImages: IGalleryImage[] = images.map(({ className, image: { url } }) => {
      return { className, url };
    });

    setGalleryImages(returnedImages);
  };

  const handleGetImagesByTypeError = (error: ApolloError) => {
    toast.error(error);
  };

  const { handleGetImagesByType, isLoading: isHandleGetImagesByType } = useGetImagesByType({
    onCompleted: handleGetImagesByTypeComplete,
    onError: handleGetImagesByTypeError,
  });

  useEffect(() => {
    handleGetImagesByType({
      type: galleryTitle,
    });
  }, [galleryTitle]);

  const isLoading = isHandleGetImagesByType;

  return (
    <>
      <Loader isLoading={Boolean(isLoading)} />
      <div id="imageContainer" className="w-full gap-3 columns-2 lg:columns-4 md:gap-5 lg:gap-6">
        {galleryImages?.map(({ url, className }, i) => (
          <div key={url} className={className}>
            <img alt={`images number ${i + 1}`} className="rounded-lg drop-shadow-md mb-2" src={url} />
          </div>
        ))}
      </div>
    </>
  );
};

export { ImageGallery };
