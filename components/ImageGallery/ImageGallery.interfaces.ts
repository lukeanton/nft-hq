export type GalleryTitleType = 'Bape';

export interface ImageGalleryProps {
  galleryTitle: GalleryTitleType;
}

export interface IGalleryImage {
  className?: string;
  url: string;
}
