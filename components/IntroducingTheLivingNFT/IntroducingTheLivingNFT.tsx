import { ModelBlock } from '../ModelBlock';
import { Paragraph } from '../Paragraph';

import { cms } from '../../constants/cms';

const IntroducingTheLivingNFT = () => {
  const { paragraph } = cms.introducingTheLivingNFT;
  return (
    <div className="my-32">
      <h2 className="text-3xl md:text-4xl lg:text-5xl mt-0 mb-6 uppercase">
        Introducing <span className="text-[#77CC8F] drop-shadow-sm">non static</span> nfts
      </h2>
      <Paragraph paragraphText={paragraph} />
      <div className="mt-16 px-3 text-center">
        <figcaption className="text-lg text-[#585e71] italic m-auto">The image below changes based off the weather in Tokyo</figcaption>
        <ModelBlock />
      </div>
    </div>
  );
};

export { IntroducingTheLivingNFT };
