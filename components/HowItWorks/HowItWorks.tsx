import { useEffect, useState } from 'react';

import { ApolloError } from '@apollo/client';
import { toast } from 'react-toastify';

import { useGetImagesByType, IGetImageByTypeOnCompletedResponse } from '../../hooks';

const HowItWorks = () => {
  const [url, setUrl] = useState<string>();

  const handleGetImagesByTypeComplete = (data?: IGetImageByTypeOnCompletedResponse) => {
    if (!data) {
      return;
    }

    const { images } = data;
    const [image] = images;
    const {
      image: { url: imageUrl },
    } = image;

    setUrl(String(imageUrl));
  };

  const handleGetImagesByTypeError = (error: ApolloError) => {
    toast.error(error);
  };

  const { handleGetImagesByType } = useGetImagesByType({
    onCompleted: handleGetImagesByTypeComplete,
    onError: handleGetImagesByTypeError,
  });

  useEffect(() => {
    handleGetImagesByType({ type: 'Diagram' });
  }, []);

  return (
    <div className="lg:my-32">
      <h2 className="text-3xl md:text-4xl lg:text-5xl mt-0 mb-6 uppercase">
        Bored apes are static... what if we could add <span className="text-[#77CC8F] drop-shadow-sm">life?</span>
      </h2>
      <img alt="how it works" className="lg:px-32 md:px-9 px-9 lg:scale-75 sm:scale-50" src={url} />
    </div>
  );
};

export { HowItWorks };
