export interface ISectionTitleProps {
  /**
   * Title text
   */
  title: string;
}
