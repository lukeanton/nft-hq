import { ISectionTitleProps } from './SectionTitle.types';

const SectionTitle = ({ title }: ISectionTitleProps) => <h2 className="text-3xl md:text-4xl lg:text-5xl mt-0 mb-6">{title}</h2>;

export { SectionTitle };
