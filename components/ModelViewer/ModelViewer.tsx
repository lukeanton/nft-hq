import { useEffect, useState } from 'react';

import { ModelViewerProps } from './ModelViewer.interfaces';

import { useMediaQuery } from '../../hooks/useMediaQuery';

const ModelViewer = ({ src }: ModelViewerProps) => {
  const isSmallScreen: boolean = useMediaQuery({ size: 'small' });

  const [isPlaying, setIsPlaying] = useState<boolean>(false);

  useEffect(() => {
    setIsPlaying(true);
  }, []);

  return (
    <div className={`flex items-center justify-center drop-shadow-xl  ${isSmallScreen ? ' scale-75  mb-[20px]' : 'scale-100'}`}>
      <video autoPlay={isPlaying} className="rounded-lg	w-[450px]" loop={isPlaying} playsInline={isPlaying} src={src} muted></video>
    </div>
  );
};
2;

export { ModelViewer };
