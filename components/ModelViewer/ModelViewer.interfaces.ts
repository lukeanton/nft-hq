import { IAdditionalClassNames } from '../../interfaces';

export interface ModelViewerProps extends IAdditionalClassNames {
  src: string;
}
