import { headlineConstants } from './Headline.constants';
import { IHeadlineProps } from './Headline.types';

import { useMediaQuery } from '../../hooks/useMediaQuery';
const Headline = ({ headlineText, headlineSize }: IHeadlineProps) => {
  const { headlineSizeTypes } = headlineConstants;
  const isSmallScreen: boolean = useMediaQuery({ size: 'small' });

  return (
    <div className="text-center absolute left-0 right-0 mb-10">
      <h1
        className={`${headlineSizeTypes[headlineSize]} ${isSmallScreen ? 'outline-1px-white' : 'outline-3px-white'} offset-8px 
      px-8 md:text-5xl lg:text-8xl lg:px-32 lg:outline-5px-white absolute`}
        id="headlineWhiteOutline"
      >
        {headlineText}
      </h1>
      <h1 className={`${headlineSizeTypes[headlineSize]} px-8 md:text-5xl lg:text-8xl lg:px-32 absolute`} id="headlineText">
        {headlineText}
      </h1>
    </div>
  );
};

export { Headline };
