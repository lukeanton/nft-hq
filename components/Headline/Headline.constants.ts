const HEADLINE_SIZE_TYPES = Object.freeze({
  small: 'text-xl',
  medium: 'text-5xl',
  large: 'text-9xl',
});

const headlineConstants = Object.freeze({
  headlineSizeTypes: HEADLINE_SIZE_TYPES,
});

export { headlineConstants };
