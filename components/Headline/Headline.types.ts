import { SizeType } from '../../types';

export interface IHeadlineProps {
  /**
   * Specify the size of the headline tezt
   */
  headlineSize: SizeType;
  /**
   * Specify the text content of the headline
   */
  headlineText: string;
}
