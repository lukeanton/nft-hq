import { ReactNode } from 'react';

export interface LoaderProps {
  icon?: ReactNode;
  isLoading: boolean;
}
