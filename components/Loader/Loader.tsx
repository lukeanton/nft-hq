import { useState, useEffect } from 'react';

import { LoaderProps } from './Loader.interfaces';

import { useGetImagesByType } from '../../hooks';

const Loader = ({ isLoading = false }: LoaderProps) => {
  const [isShowScreen, setIsShowScreen] = useState<boolean>(true);
  const [isShow, setIsShow] = useState<boolean>(true);
  const [fadeoutTime] = useState<number>(0.9);

  const style = { transition: `visibility 0s ${fadeoutTime}s, opacity ${fadeoutTime}s linear` };

  const [imgUrl, setImageUrl] = useState<string>();

  const { handleGetImagesByType } = useGetImagesByType({
    onCompleted: ({ images }) => {
      const [image] = images;
      const {
        image: { url: imageUrl },
      } = image;

      setImageUrl(String(imageUrl));
    },
  });

  useEffect(() => {
    handleGetImagesByType({ type: 'animated-logo' });
  }, []);

  const handleFadeOut = () => {
    setIsShow(false);
    setTimeout(() => {
      setIsShowScreen(false);
    }, Math.trunc(fadeoutTime * 1000));
  };

  useEffect(() => {
    if (!isLoading) {
      handleFadeOut();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading]);

  return isShowScreen ? (
    <div
      aria-live="assertive"
      className={`items-center bg-white text-white flex flex-col justify-center left-0 fixed bottom-0 right-0 top-0  z-50 ${
        isShow ? 'opacity-100' : 'opacity-0'
      }`}
      role="alert"
      style={style}
    >
      <div className="animate-pulse" aria-hidden>
        <object aria-label="logo" data={imgUrl} />

        <p className="h-hide-visually">Loading...</p>
      </div>
    </div>
  ) : null;
};

export { Loader };
