export interface ILoadingScreenProps {
  /**
   * Seconds for the fadeout
   */
  fadeoutTime: number;
  /**
   * Bool for once all loading is completed
   */
  isLoading: boolean;
}
