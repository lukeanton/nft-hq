import { useState, useEffect } from 'react';

import { ImageLogo } from '../ImageLogo';

import { ILoadingScreenProps } from './LoadingScreen.types';

import { pageConstants } from '../../constants/page';

const LoadingScreen = ({ isLoading, fadeoutTime }: ILoadingScreenProps) => {
  const { saucyEyeConspiracyTheroryParanoiaIsTakingOverIThinkIThinkIAmInLoveWithPutinsMomLoader } = pageConstants;

  const [isShowScreen, setIsShowScreen] = useState<boolean>(true);
  const [show, setShow] = useState<number>(1);

  const fadeOutLoadingScreen = () => {
    setShow(0);
    setTimeout(() => {
      setIsShowScreen(false);
    }, Math.trunc(fadeoutTime * 1000));
  };

  const style = {
    transition: `visibility 0s ${fadeoutTime}s, opacity ${fadeoutTime}s linear`,
  };

  useEffect(() => {
    if (isLoading) fadeOutLoadingScreen();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading]);

  if (isShowScreen) {
    return (
      <div
        className={`animate-fade h-screen w-screen grid place-items-center fixed top-0 bottom-0 bg-white ${
          show ? 'opacity-80' : 'opacity-0'
        }`}
        id="loadingScreen"
        style={style}
      >
        <div className="flex flex-col items-center">
          <div className="animate-pulse" style={{ width: '100px' }}>
            <ImageLogo size="large" src={saucyEyeConspiracyTheroryParanoiaIsTakingOverIThinkIThinkIAmInLoveWithPutinsMomLoader} />
          </div>
        </div>
      </div>
    );
  }

  return <></>;
};

export { LoadingScreen };
