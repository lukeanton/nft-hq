const DividerBar = () => <div className="my-16 lg:my-20 mx-auto h-1 w-1/2 bg-gradient-to-r from-green-500 to-cyan-400" />;

export { DividerBar };
