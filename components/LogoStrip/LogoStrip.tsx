import React, { useEffect, useState } from 'react';

import { ImageLogo } from '../ImageLogo';
import { Loader } from '../Loader';

import { useGetImagesByType } from '../../hooks';
import { useMediaQuery } from '../../hooks/useMediaQuery';

const LogoStrip = () => {
  const isLargeScreen: boolean = useMediaQuery({ size: 'large' });
  const smallScreenFontSize = !isLargeScreen ? 'text-lg' : undefined;
  const [imgUrl, setImageUrl] = useState<string>();

  const { handleGetImagesByType, isLoading: isHandleGetImagesByTypeLoading } = useGetImagesByType({
    onCompleted: ({ images }) => {
      const [image] = images;
      const {
        image: { url: imageUrl },
      } = image;

      setImageUrl(String(imageUrl));
    },
  });

  useEffect(() => {
    handleGetImagesByType({ type: 'animated-logo-white' });
  }, []);

  const isLoading = isHandleGetImagesByTypeLoading;

  return (
    <div className="relative text-center w-full content-center py-8 pt-8">
      <Loader isLoading={Boolean(isLoading)} />
      <span className="inline-flex items-center text-2xl ">
        {imgUrl && <ImageLogo additionalClassNames="pr-1 mr-1" size="medium" src={imgUrl} />}
        <span className={`${String(smallScreenFontSize)} mr-2`}> LIVING </span>
        <span className={`${String(smallScreenFontSize)} font-bold text-white	mr-1`}> ART </span>
        <span className={`${String(smallScreenFontSize)}`}> LABS </span>
      </span>
    </div>
  );
};

export { LogoStrip };
