import { Headline } from '../Headline';
import { LogoStrip } from '../LogoStrip';

import { useMediaQuery } from '../../hooks/useMediaQuery';

const HeroPanel = () => {
  const isLargeScreen: boolean = useMediaQuery({ size: 'large' });
  const headlineSize = isLargeScreen ? 'large' : 'medium';

  return (
    <section id="heroPanel">
      <div className="bg-gradient-to-r from-green-500 to-cyan-400 w-full h-1/2 text-center aspect-auto absolute"></div>
      <div className="bg-gradient-to-t from-white border-b-2 border-b-white to-transparent w-full h-1/2 text-center aspect-au absolute"></div>
      <LogoStrip />
      <Headline headlineSize={headlineSize} headlineText={`NFT ART DOESN'T HAVE TO BE STATIC AND LIFELESS.`} />
    </section>
  );
};

export { HeroPanel };
