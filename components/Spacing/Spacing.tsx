import { SpacingProps } from './Spacing.types';

/**
 * For creating vertical space between components.
 */
const Spacing: React.FC<SpacingProps> = ({ children, size }) => <div className={size ? `h-spacing-${size}` : 'h-spacing'}>{children}</div>;

export { Spacing };
