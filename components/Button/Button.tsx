import { IButtonProps } from './Button.types';

const Button = ({ isDisabled = false, linkButton, onClick, text, type = 'button', variant = 'primary' }: IButtonProps) => {
  const ButtonComponent = linkButton ? linkButton.linkComponent ?? 'a' : 'button';

  return (
    <ButtonComponent
      className={`c-button c-button--${variant}`}
      data-testid="qa-button"
      disabled={isDisabled}
      href={linkButton?.url}
      id={linkButton?.id}
      rel={linkButton?.url.indexOf('http') === 0 ? 'noopener noreferrer' : undefined}
      target={linkButton?.target}
      type={type}
      onClick={onClick}
    >
      {text}
    </ButtonComponent>
  );
};

export { Button };
