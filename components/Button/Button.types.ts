import { ElementType } from 'react';

import { IAdditionalClassNames, IIsDisabled } from '../../interfaces';
import { ButtonType, ButtonVariantType, TargetType } from '../../types';

export interface ILinkButton {
  /**
   * Specify an id to identify the anchor tag
   */
  id: string;
  /**
   * Provide a link element that will be used in place of the a tag
   */
  linkComponent?: ElementType;
  /**
   * Where to display the linked URL, as the name for a browsing context (a tab, window, or `<iframe>`)
   */
  target?: TargetType;
  /**
   * Specify the URL that the hyperlink points to
   */
  url: string;
}

export interface IButtonProps extends IAdditionalClassNames, IIsDisabled {
  /**
   * Provide a link to use the button to navigate pages
   */
  linkButton?: ILinkButton;
  /**
   * Provide an onClick function that will be triggered when the button is clicked
   */
  onClick?: () => void;
  /**
   * Specify the text content of the button
   */
  text: string;
  /**
   * Specify the type of button you would like to display
   */
  type?: ButtonType;
  /**
   * Specify the variant of button you would like to display
   */
  variant?: ButtonVariantType;
}
