import { IconProps } from './Icon.types';

/**
 * Used to insert an SVG icon into other components.
 */
const Icon = ({ additionalClassNames, hasAriaHidden = true, iconId }: IconProps) => (
  <svg aria-hidden={hasAriaHidden} className={additionalClassNames} focusable="false">
    <use xlinkHref={`#${iconId}`} />
  </svg>
);

export { Icon };
