import { IAdditionalClassNames } from '../../interfaces';
import { IconIdType } from '../../types';

export interface IconProps extends IAdditionalClassNames {
  /**
   * The boolean value that decides wether the aria is hidden
   */
  hasAriaHidden?: boolean;
  /**
   * Specify the icon id for the icon you would like to display
   */
  iconId: IconIdType;
}
