const IMAGE_SIZES = Object.freeze({
  small: 15,
  medium: 30,
  large: 60,
});

const imageLogoConstants = Object.freeze({
  imageSizes: IMAGE_SIZES,
});

export { imageLogoConstants };
