import { imageLogoConstants } from './ImageLogo.constants';
import { ImageImageLogoProps } from './ImageLogo.types';

const ImageLogo = ({ size, src }: ImageImageLogoProps) => {
  const { imageSizes } = imageLogoConstants;
  const imageSize = imageSizes[size];

  return <object aria-label="logo" className={`mx-auto mx-2"`} height={imageSize} data={src} width={imageSize} />;
};

export { ImageLogo };
