import { IAdditionalClassNames } from '../../interfaces';
import { SizeType } from '../../types';

export interface ImageImageLogoProps extends IAdditionalClassNames {
  /**
   * small medium or large, whats in going to be?
   */
  size: SizeType;
  /**
   * Specify the color
   */
  src: string;
}
