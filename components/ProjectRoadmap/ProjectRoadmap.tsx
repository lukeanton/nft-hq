import { RoadmapLine } from '../RoadmapLine';
import { SectionTitle } from '../SectionTitle';

import { cms } from '../../constants/cms';

const projects = [
  {
    imgUrl: 'https://www.fillmurray.com//600/800',
    title: 'WEATHER PROJECT',
    year: 'Early 2022',
  },
  {
    imgUrl: 'https://www.fillmurray.com//600/800',
    title: 'HOLIDAY MAN',
    year: 'Early 2022',
  },
  {
    imgUrl: 'https://www.fillmurray.com//600/800',
    title: 'LETS COLLABORATE',
    year: 'Early 2022',
  },
];

const ProjectRoadmap = () => {
  return (
    <>
      <SectionTitle title={cms.projectRoadmap.title} />
      <div className="mt-20">
        {projects.map((p, i) => {
          return <RoadmapLine key={i} imageUrl={p.imgUrl} index={i} title={p.title} year={p.year} />;
        })}
      </div>
    </>
  );
};

export { ProjectRoadmap };
