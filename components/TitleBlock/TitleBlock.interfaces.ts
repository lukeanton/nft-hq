import { IConditionConfig } from '../ModelBlock';

export interface IWeatherDetails {
  conditionConfiguration?: IConditionConfig;
  humidity: string;
  minMaxTemp: string;
  temperature: string;
  title: string;
  weatherType: string;
}

export interface TitleBlockProps {
  additionalClassNames?: string;
  weatherDetails: IWeatherDetails;
}
