import { Icon } from '../Icon';
import { IConditionConfig } from '../ModelBlock';

import { TitleBlockProps } from './TitleBlock.interfaces';

import { useMediaQuery } from '../../hooks/useMediaQuery';
import { IconIdType } from '../../types';

const TitleBlock = ({ additionalClassNames, weatherDetails }: TitleBlockProps) => {
  const isSmallScreen: boolean = useMediaQuery({ size: 'small' });

  const { title, conditionConfiguration, weatherType, temperature, humidity } = weatherDetails;
  const { iconId, conditionTitle } = conditionConfiguration ?? ({} as IConditionConfig);

  const titleBlockStyles = `${String(
    additionalClassNames,
  )} bg-slate-50 drop-shadow-xl font-raleway text-[#585e71] text-center w-80 translate-y-[-2rem] rounded-lg px-5  ${
    isSmallScreen ? ' scale-50 mt-[-130px] mb-[-100px]' : 'scale-100'
  }`;

  return (
    <div className={titleBlockStyles}>
      <div className="flex items-center justify-center mt-3">
        <div className="uppercase tracking-widest tracking-[.25em] ml-5 mr-1">{title}</div>
        <Icon additionalClassNames="w-5 h-5 " iconId={`${String(iconId)}_icon` as IconIdType} />
      </div>

      <div className="text-xs tracking-widest font-normal mb-5">{weatherType}</div>

      <div className="flex justify-between w-full">
        <span className="text-sm font-bold m-0"> Conditions</span>
        <span className="m-0 flex items-center capitali">
          {conditionTitle} <Icon additionalClassNames="w-3 h-3 ml-1" iconId={`${String(iconId)}_icon` as IconIdType} />
        </span>
      </div>

      <div className="flex justify-between w-full">
        <span className="text-sm font-bold m-0"> Temperature</span>
        <span className="m-0"> {temperature}° C</span>
      </div>

      <div className="flex justify-between w-full">
        <span className="text-sm font-bold m-0">Humidity</span>
        <span className="m-0"> {humidity}%</span>
      </div>
      <br></br>
    </div>
  );
};

export { TitleBlock };
