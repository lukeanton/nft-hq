import { IRoadmapLine } from './RoadmapLine.types';

const RoadmapLine = ({ index, imageUrl, title, year }: IRoadmapLine) => {
  const isOddRow = index % 2 === 0;
  return (
    <div className={`py-2 px-2 w-full content-center text-center bg-white`}>
      <div className="inline-flex">
        <div className="w-32 lg:w-96 text-right  py-8 lg:py-16">
          {isOddRow && (
            <>
              <h2 className="text-lg lg:text-2xl leading-4">{title}</h2>
              <h2>{year}</h2>
            </>
          )}
        </div>
        <div
          className={`rounded-full h-20 w-20 lg:h-40 lg:w-40 lg:rounded-full bg-cover bg-center mx-4 my-auto slideAcross`}
          style={{ backgroundImage: `url(${imageUrl})` }}
        ></div>
        <div className="w-32 lg:w-96 text-left py-8 lg:py-16">
          {!isOddRow && (
            <>
              <h2 className="text-lg lg:text-2xl leading-4">{title}</h2>
              <h2>{year}</h2>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export { RoadmapLine };
