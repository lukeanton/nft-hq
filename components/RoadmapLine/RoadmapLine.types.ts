export interface IRoadmapLine {
  /**
   * Image url
   */
  imageUrl: string;
  /**
   * Index that dictates the color of the background
   */
  index: number;
  /**
   * Project title ie 'The Cool Project'
   */
  title: string;
  /**
   * Project year ie 'Mid 2023'
   */
  year: string;
}
