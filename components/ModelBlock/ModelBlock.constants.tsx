import { dateTimeToNumber } from './ModelBolck.helpers';
const {
  env: { OPEN_WEATHER_BASE_URL },
} = process;

const DEFAULT_TIME_OF_DAY = 'morning';
const TOKYO_GEO_LOCATION = `lat=${35.6762}&lon=${139.6503}`;

const TOKYO_WEATHER_ENDPOINT = `https://api.openweathermap.org/data/2.5/weather?${TOKYO_GEO_LOCATION}&appid=${String(
  OPEN_WEATHER_BASE_URL,
)}&units=metric`;

const JAPANESE_DATE = new Date().toLocaleTimeString('ja-JP', {
  hour: '2-digit',
  minute: 'numeric',
  timeZone: 'Asia/Tokyo',
});

const JAPANESE_TIME_2_NUMBER = dateTimeToNumber(JAPANESE_DATE);

// TODO: Create propper types for these guys
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const WEATHER_CONDITION_TYPES: any = Object.freeze({
  Clear: 'clear',
  Clouds: 'clear',
  Drizzle: 'rain',
  Rain: 'rain',
  Snow: 'snow',
  Thunderstorm: 'rain',
});

const modelBlockConstants = Object.freeze({
  defaultTimeOfDay: DEFAULT_TIME_OF_DAY,
  japaneseTime2Number: JAPANESE_TIME_2_NUMBER,
  tokyoWeatherEndpoint: TOKYO_WEATHER_ENDPOINT,
  weatherConditionTypes: WEATHER_CONDITION_TYPES,
});

export { modelBlockConstants };
