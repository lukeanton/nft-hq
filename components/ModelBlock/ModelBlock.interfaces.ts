import { IconIdType } from '../../types';

export interface IConditionConfig {
  conditionTitle: string;
  iconId?: IconIdType;
}
