/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';

import { ApolloError } from '@apollo/react-hooks';
import { toast } from 'react-toastify';

import { ImageLogo } from '../ImageLogo';
import { Loader } from '../Loader';
import { ModelViewer } from '../ModelViewer';
import { IWeatherDetails, TitleBlock } from '../TitleBlock';

import { modelBlockConstants } from './ModelBlock.constants';
import { IConditionConfig } from './ModelBlock.interfaces';
import { getTimeOfDay } from './ModelBolck.helpers';

import { pageConstants } from '../../constants/page';
import { useGetModelByTimeAndCondition, useBobbaFetch, IGetModelByTimeAndConditionOnCompletedResponse } from '../../hooks';

const ModelBlock = () => {
  const { saucyEyeConspiracyTheroryParanoiaIsTakingOverIThinkIThinkIAmInLoveWithPutinsMomLoader } = pageConstants;
  const { weatherConditionTypes, tokyoWeatherEndpoint, japaneseTime2Number } = modelBlockConstants;
  const { data: weatherData, isLoading: isWeatherDataLoading } = useBobbaFetch({ endpoint: tokyoWeatherEndpoint, method: 'GET' });

  const [weatherDetails, setWeatherDetails] = useState<IWeatherDetails>({} as IWeatherDetails);
  const [weatherType, setWeatherType] = useState<string>('');
  const [humidity, setHumidity] = useState<string>('');
  const [minMaxTemp, setMinMaxTemp] = useState<string>('');
  const [conditionConfiguration, setConditionConfiguration] = useState<IConditionConfig>();
  const [modelUrl, setModelUrl] = useState<string>('');
  const [temperature, setTemperature] = useState<string>('');

  const handleGetModelByTimeAndConditionCompleted = (data?: IGetModelByTimeAndConditionOnCompletedResponse) => {
    if (!data) {
      return;
    }

    const {
      model: {
        title,
        cityModel: { url },
      },
    } = data;

    const details: IWeatherDetails = Object.freeze({
      title,
      conditionConfiguration,
      weatherType,
      temperature,
      minMaxTemp,
      humidity,
    });

    setWeatherDetails(details);
    setModelUrl(url);
  };

  const handleGetModelByTimeAndConditionError = (error: ApolloError) => {
    toast.error(error);
  };

  const { handleGetModelByTimeAndCondition, isLoading: isHandleGetModelByTimeAndCondition } = useGetModelByTimeAndCondition({
    onCompleted: handleGetModelByTimeAndConditionCompleted,
    onError: handleGetModelByTimeAndConditionError,
  });

  useEffect(() => {
    if (!weatherData) {
      return;
    }

    const {
      main: { temp, humidity: humidityPercentage, temp_min, temp_max },
      weather,
    } = weatherData;
    const [weatherValues] = weather;
    const { main, description: weatherDescription } = weatherValues;

    const weatherCondition = weatherConditionTypes[main];
    const conditionConfig = { iconId: weatherCondition, conditionTitle: weatherDescription };

    const timeOfDay = getTimeOfDay(japaneseTime2Number);
    const minMaxString = `${String(temp_min)} - ${String(temp_max)}`;

    setConditionConfiguration(conditionConfig);
    setWeatherType(String(weatherCondition));
    setHumidity(String(humidityPercentage));
    setMinMaxTemp(minMaxString);
    setTemperature(String(temp));

    handleGetModelByTimeAndCondition({
      weatherCondition,
      timeOfDay,
    });
  }, [weatherData]);

  const isLoading = isWeatherDataLoading || isHandleGetModelByTimeAndCondition;

  return (
    <>
      <Loader
        icon={
          <ImageLogo
            additionalClassNames="animate-pulse"
            size="large"
            src={saucyEyeConspiracyTheroryParanoiaIsTakingOverIThinkIThinkIAmInLoveWithPutinsMomLoader}
          />
        }
        isLoading={Boolean(isLoading)}
      />
      <ModelViewer src={modelUrl} />
      <div className="flex flex-col items-center justify-center">
        <TitleBlock weatherDetails={weatherDetails} />
      </div>
    </>
  );
};

export { ModelBlock };
