import { modelBlockConstants } from './ModelBlock.constants';

export const dateTimeToNumber = (date: string) => {
  const split = date.split(':');

  return Number(split[0]);
};

export const getTimeOfDay = (time: number) => {
  const { defaultTimeOfDay } = modelBlockConstants;

  if (time >= 0 && time < 11) {
    return 'morning';
  }
  if (time >= 11 && time < 17) {
    return 'day';
  }
  if (time >= 17 && time <= 24) {
    return 'evening';
  }
  return defaultTimeOfDay;
};
