export type ButtonType = 'button' | 'submit' | 'reset';

export type ButtonVariantType = 'primary' | 'secondary' | 'tertiary';

export type IconIdType = 'like_icon' | 'clear_icon' | 'rain_icon' | 'snow_icon';

export type MethodType = 'GET' | 'POST' | 'PUT' | 'DELETE';

export type SizeType = 'small' | 'medium' | 'large';

export type SpacingSizeType =
  | '6x-large'
  | '5x-large'
  | '4x-large'
  | '3x-large'
  | '2x-large'
  | 'large'
  | 'x-large'
  | 'small'
  | 'x-small'
  | '2x-small'
  | '3x-small'
  | 'none';

export type TargetType = '_blank' | '_self' | '_parent' | 'top';

export type ImageSrcType = '/static/logoWhite.svg';
