export const cms = {
  loadingMessage: 'Wu han or wu who? ... huh? what... is that you jerry? 😱!!!!! ',
  heroTitle: "NFT ART DOESN'T HAVE TO BE STATIC AND LIFELESS.",
  introducingTheLivingNFT: {
    title: 'INTRODUCING THE LIVING NFT.',
    imageTag: 'This is a LIVING NFT!',
    paragraph:
      'Imagine a piece of art that changes with the tide, or one that ages over time. This technology is already available to this and explore, drive and push this new form of media into the future',
  },
  aboutUs: {
    title: 'ABOUT US.',
    paragraph1:
      'We are a small and growing community of creative developers and artists that want to breathe life into static NFTs. Our goal is to collaborate with others an create unique works that are truly living pieces of art.',
    paragraph2:
      'By leveraging metadata and custom-built API’s hosted on the blockchain we aim to create a series of limited proof-of-concepts as well as collaborative pieces that are truly alive. We are pushing forward and pushing boundaries of the living NFT.',
  },
  projectRoadmap: {
    title: 'ROADMAP',
  },
};
