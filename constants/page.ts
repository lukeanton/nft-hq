const LOGO_SRC = '/static/lab-logo.svg';
const SAUCY_EYE_CONSPIRACY_THEORY_PARANOIA_IS_TAKING_OVER_I_THINK_I_AM_IN_LOVE_WITH_PUTINS_MOM_LOADER = '/static/lab-logo.svg';

const pageConstants = Object.freeze({
  logoSrc: LOGO_SRC,
  saucyEyeConspiracyTheroryParanoiaIsTakingOverIThinkIThinkIAmInLoveWithPutinsMomLoader:
    SAUCY_EYE_CONSPIRACY_THEORY_PARANOIA_IS_TAKING_OVER_I_THINK_I_AM_IN_LOVE_WITH_PUTINS_MOM_LOADER,
});

export { pageConstants };
