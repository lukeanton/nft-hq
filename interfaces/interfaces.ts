import { SyntheticEvent } from 'react';

import { ApolloError } from '@apollo/client';

//I have ordered these in alpha

export interface IAdditionalClassNames {
  /**
   * Specify additional class names that apply to the parent node
   */
  additionalClassNames?: string;
}

export interface IApolloResponseVariables {
  error?: ApolloError;
  isLoading?: boolean;
}

export interface IIsDisabled {
  /**
   * The boolean value that decides wether the element is disabled
   */
  isDisabled?: boolean;
}

export interface IOnClickButton {
  onClick: (event: SyntheticEvent<HTMLButtonElement>) => void;
}

export interface IOnCompleted<T> {
  onCompleted?: (data: T) => void;
}

export interface IOnError<T> {
  onError?: (error: T) => void;
}

export interface ISupportiveText {
  /**
   * Specify supportive text for screen readers
   */
  supportiveText: string;
}
