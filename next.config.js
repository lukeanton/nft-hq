/** @type {import('next').NextConfig} */
module.exports = {
  env: {
    CONTENT_URL: process.env.CONTENT_URL,
    MY_PROFILE_ID: process.env.MY_PROFILE_ID,
    AUTH_TOKEN: process.env.AUTH_TOKEN,
    OPEN_WEATHER_BASE_URL: process.env.OPEN_WEATHER_BASE_URL,
  },
  reactStrictMode: true,
};
