import { MethodType } from '../../types';

export interface IHandleFetchParams {}

export interface IFetchResponse {
  response: any;
}

export interface IUseBobbaFetch {
  data: any;
  isLoading: boolean;
}

export interface IFetchOptions {
  endpoint: string;
  method: MethodType;
}
