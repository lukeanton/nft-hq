import { useEffect, useState } from 'react';

import { IFetchOptions, IUseBobbaFetch } from './useBobbaFetch.interfaces';
import { toast } from 'react-toastify';
import { useBobbaFetchConstants } from './useBobbaFetch.constants';

const useBobbaFetch = (options?: IFetchOptions): IUseBobbaFetch => {
  const { endpoint, method } = options ?? ({} as IFetchOptions);

  const [data, setData] = useState<any>();
  const [isLoading, setIsLoading] = useState(true);

  const { statusMessages } = useBobbaFetchConstants;

  const executeFetch = async () => {
    await fetch(endpoint, { method })
      .then((response) => {
        //TODO: This is hella annoying tbh might handle responses a little differently
        // const { status } = response;
        // const toastMessage: string = statusMessages[status];
        // toast.success(toastMessage);
        setIsLoading(false);

        return response.json();
      })
      .then((responseData) => {
        return setData(responseData);
      })
      .catch(({ message }) => {
        toast.error(message);
      });
  };

  useEffect(() => {
    executeFetch();
  }, []);

  return {
    data,
    isLoading,
  };
};

export { useBobbaFetch };
