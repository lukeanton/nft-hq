const STATUS_MESSAGES: any = Object.freeze({
  404: "Dang dog! I can't find it for you",
  200: 'Dang my broskie, deeze gotta be the best pies in Redfern 🥧',
});

const useBobbaFetchConstants = Object.freeze({
  statusMessages: STATUS_MESSAGES,
});

export { useBobbaFetchConstants };
