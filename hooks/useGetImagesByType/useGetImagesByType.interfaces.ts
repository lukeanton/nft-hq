import { ApolloError, DocumentNode } from '@apollo/react-hooks';

import { IApolloResponseVariables } from '../useGetModelByTimeAndCondition';

import { IOnCompleted, IOnError } from '../../interfaces';

export interface IImage {
  url: string;
}

export interface IDBImage {
  className?: string;
  image: IImage;
  url?: string;
}

export interface IHandleGetImageByTypeParams extends IGetImageByTypeQueryVariables {}

export interface IGetImageByTypeGraphQLResponse {
  images: IDBImage[];
}

export interface IGetImageByTypeQueryVariables {
  type: string;
}

export interface IGetImageByTypeOnCompletedResponse {
  images: IDBImage[];
}

export interface IUseGetImageByType extends IApolloResponseVariables {
  handleGetImagesByType: (params: IHandleGetImageByTypeParams) => void;
}

export interface IUseGetImageByTypeOptions extends IOnCompleted<IGetImageByTypeOnCompletedResponse>, IOnError<ApolloError> {
  query?: DocumentNode;
}
