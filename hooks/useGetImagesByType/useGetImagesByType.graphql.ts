import { gql } from '@apollo/react-hooks';

export const GET_IMAGES_BY_TYPE = gql`
  query getImages($type: String!) {
    images(where: { type: $type }) {
      image {
        url
      }
      className
    }
  }
`;
