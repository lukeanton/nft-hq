import { useLazyQuery } from '@apollo/client';

import { GET_IMAGES_BY_TYPE } from './useGetImagesByType.graphql';
import { IHandleGetImageByTypeParams, IUseGetImageByType, IUseGetImageByTypeOptions } from './useGetImagesByType.interfaces';

import { client } from '../../middleware/client';

const useGetImagesByType = (options?: IUseGetImageByTypeOptions): IUseGetImageByType => {
  const { query, onCompleted, onError } = options ?? ({} as IUseGetImageByTypeOptions);

  const [executeGetGallery, { loading: isLoading }] = useLazyQuery(query ?? GET_IMAGES_BY_TYPE, {
    client,
    onCompleted: (data) => {
      if (!onCompleted) {
        return;
      }

      const { images } = data;

      onCompleted({
        images,
      });
    },
    onError,
  });

  const handleGetImagesByType = async ({ type }: IHandleGetImageByTypeParams) => {
    await executeGetGallery({
      variables: {
        type,
      },
    });
  };

  return {
    handleGetImagesByType,
    isLoading,
  };
};

export { useGetImagesByType };
