import { ApolloError, DocumentNode } from '@apollo/react-hooks';

export interface IApolloResponseVariables {
  error?: ApolloError;
  isLoading?: boolean;
}

export interface IOnCompleted<T> {
  onCompleted?: (data: T) => void;
}
export interface IOnError<T> {
  onError?: (error: T) => void;
}

export interface IDBCityModel {
  url: string;
  fileName: string;
  id: number;
  mimeType: string;
}

export interface IDBModel {
  author: string;
  cityModel: IDBCityModel;
  description: string;
  id: number;
  title: string;
}

export interface IHandleGetModelByTimeAndConditionParams extends IGetModelByTimeAndConditionQueryVariables {}

export interface IGetModelByTimeAndConditionQueryGraphQLResponse {
  models: IDBModel[];
}

export interface IGetModelByTimeAndConditionQueryVariables {
  weatherCondition: string;
  timeOfDay: string;
}

export interface IGetModelByTimeAndConditionOnCompletedResponse {
  model: IDBModel;
}

export interface IUseGetModelByTimeAndCondition extends IApolloResponseVariables {
  handleGetModelByTimeAndCondition: (params: IHandleGetModelByTimeAndConditionParams) => void;
}

export interface IUseGetModelByTimeAndConditionOptions
  extends IOnCompleted<IGetModelByTimeAndConditionOnCompletedResponse>,
    IOnError<ApolloError> {
  query?: DocumentNode;
}
