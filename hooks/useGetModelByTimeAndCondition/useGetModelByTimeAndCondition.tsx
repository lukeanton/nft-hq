import { useLazyQuery } from '@apollo/client';

import { GET_MODEL_BY_TIME_AND_CONDITION } from './useGetModelByTimeAndCondition.graphql';
import {
  IHandleGetModelByTimeAndConditionParams,
  IUseGetModelByTimeAndCondition,
  IUseGetModelByTimeAndConditionOptions,
} from './useGetModelByTimeAndCondition.interfaces';

import { client } from '../../middleware/client';

const useGetModelByTimeAndCondition = (options?: IUseGetModelByTimeAndConditionOptions): IUseGetModelByTimeAndCondition => {
  const { query, onCompleted, onError } = options ?? ({} as IUseGetModelByTimeAndConditionOptions);

  const [executeGetUserFlowAccessMode, { loading: isLoading }] = useLazyQuery(query ?? GET_MODEL_BY_TIME_AND_CONDITION, {
    client,
    onCompleted: (data) => {
      if (!onCompleted) {
        return;
      }

      const { getModelByTimeAndCondition } = data;

      const [model] = getModelByTimeAndCondition;

      onCompleted({
        model,
      });
    },
    onError,
  });

  const handleGetModelByTimeAndCondition = async ({ weatherCondition, timeOfDay }: IHandleGetModelByTimeAndConditionParams) => {
    await executeGetUserFlowAccessMode({
      variables: {
        weatherCondition,
        timeOfDay,
      },
    });
  };

  return {
    handleGetModelByTimeAndCondition,
    isLoading,
  };
};

export { useGetModelByTimeAndCondition };
