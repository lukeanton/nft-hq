import { gql } from '@apollo/react-hooks';

export const GET_MODEL_BY_TIME_AND_CONDITION = gql`
  query getModelByTimeAndCondition($timeOfDay: String!, $weatherCondition: String!) {
    getModelByTimeAndCondition: models(where: { timeOfDay: $timeOfDay, weatherCondition: $weatherCondition }) {
      author
      cityModel {
        url
        fileName
        id
        mimeType
      }
      description
      id
      title
    }
  }
`;
