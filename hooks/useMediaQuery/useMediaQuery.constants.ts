const SCREEN_SIZES = Object.freeze({
  small: '(max-width: 768px)',
  medium: '(max-width: 1200px)',
  large: '(min-width: 1200px)',
});

export { SCREEN_SIZES };
